const escape = require("../../../lib/rules/escape");
const { RuleTester } = require("eslint");

const ruleTester = new RuleTester({ parserOptions: { ecmaVersion: 2015 } });
const raw = String.raw;

ruleTester.run("escape", escape, {
  valid: [
    { code: "'Only ASCII and prïntåblé here'" },
    { code: "'Only ASCII and prïntåblé here'", options: ["non-printable"] },
    { code: "'\\\n\t'", options: ["non-printable"] },
    { code: raw`'\0\xe5\u200b'`, options: ["non-printable"] },
    { code: "'\\\r'", options: ["non-printable"] },
    { code: "`Only ASCII and prïntåblé here`", options: ["non-printable"] },
    { code: "`\n\t`", options: ["non-printable"] },
    { code: "'🏳️‍🌈'", options: ["non-printable"] },

    { code: "'Only ASCII here'", options: ["non-ascii"] },
    { code: "'\\\n\t'", options: ["non-ascii"] },
    { code: raw`'\0\xe5\u200b'`, options: ["non-ascii"] },
    { code: "'\\\r'", options: ["non-ascii"] },
    { code: "`Only ASCII here`", options: ["non-ascii"] },
    { code: "`\n\t`", options: ["non-ascii"] },

    {
      code: "`\n\\n abc\\0\\xe5\\u200b`",
      options: ["non-ascii", { exact: true }],
    },
  ],

  invalid: [
    {
      code: "'\0'",
      output: raw`'\0'`,
      options: [],
      errors: [raw`Unexpected non-printable character \0`],
    },
    {
      code: "'\0'",
      output: raw`'\0'`,
      options: ["non-printable"],
      errors: [raw`Unexpected non-printable character \0`],
    },
    {
      code: "'\x007'",
      output: raw`'\x007'`,
      options: ["non-printable"],
      errors: [raw`Unexpected non-printable character \0`],
    },
    {
      code: "'\u202f'",
      output: raw`'\u202f'`,
      options: ["non-printable"],
      errors: [raw`Unexpected non-printable character \u202f`],
    },
    {
      code: "'\ue000\uf4de\u{faded}\u{100000}\u{10fade}'",
      output: raw`'\ue000\uf4de\u{faded}\u{100000}\u{10fade}'`,
      options: ["non-printable"],
      errors: [
        raw`Unexpected non-printable characters \ue000, \uf4de, \u{faded}, \u{100000}, \u{10fade}`,
      ],
    },
    {
      code: "`\\\\ \n \r \r\n \v \t \b \f`",
      output: "`\\\\ \n \n \n \\v \t \\b \\f`",
      options: ["non-printable"],
      errors: [raw`Unexpected non-printable characters \v, \b, \f`],
    },

    {
      code: "'åäö'",
      output: raw`'\xe5\xe4\xf6'`,
      options: ["non-ascii"],
      errors: [raw`Unexpected non-ASCII characters \xe5, \xe4, \xf6`],
    },
    {
      code: "'\xff'",
      output: raw`'\xff'`,
      options: ["non-ascii"],
      errors: [raw`Unexpected non-ASCII character \xff`],
    },
    {
      code: "'\u0100'",
      output: raw`'\u0100'`,
      options: ["non-ascii"],
      errors: [raw`Unexpected non-ASCII character \u0100`],
    },
    {
      code: "`åäö`",
      output: "`\\xe5\\xe4\\xf6`",
      options: ["non-ascii"],
      errors: [raw`Unexpected non-ASCII characters \xe5, \xe4, \xf6`],
    },
    {
      code: "`å${ä}ö`",
      output: "`\\xe5${ä}\\xf6`",
      options: ["non-ascii"],
      errors: [
        raw`Unexpected non-ASCII character \xe5`,
        raw`Unexpected non-ASCII character \xf6`,
      ],
    },
    {
      code: "tag`å${ä}o`",
      output: "tag`\\xe5${ä}o`",
      options: ["non-ascii"],
      errors: [raw`Unexpected non-ASCII character \xe5`],
    },
    {
      code: "tag`a${ä}ö`",
      output: "tag`a${ä}\\xf6`",
      options: ["non-ascii"],
      errors: [raw`Unexpected non-ASCII character \xf6`],
    },
    {
      code: "tag`å${ä}ö`",
      output: "tag`\\xe5${ä}\\xf6`",
      options: ["non-ascii"],
      errors: [
        raw`Unexpected non-ASCII character \xe5`,
        raw`Unexpected non-ASCII character \xf6`,
      ],
    },
    {
      code: "'🏳️‍🌈'",
      output: raw`'\u{1f3f3}\ufe0f\u200d\u{1f308}'`,
      options: ["non-ascii"],
      errors: [
        raw`Unexpected non-ASCII characters \u{1f3f3}, \ufe0f, \u200d, \u{1f308}`,
      ],
    },
    {
      // Lone surrogate.
      code: "'\ud83c'",
      output: raw`'\ud83c'`,
      options: ["non-ascii"],
      errors: [raw`Unexpected non-ASCII character \ud83c`],
    },
    {
      code: "'\u{100000}'",
      output: raw`'\u{100000}'`,
      options: ["non-ascii"],
      errors: [raw`Unexpected non-ASCII character \u{100000}`],
    },

    {
      code: "'\\u0000\\uF0Fe'",
      output: "'\\0\\uf0fe'",
      options: ["non-printable", { exact: true }],
      errors: ["Unexpected escape formatting"],
    },

    {
      code: "`\n\\n\\x20\\x41 + B · Å_\\xC4 - \\x00\0\x001 \\' '`",
      output: "`\n\\n A + B · Å_Ä - \\0\\0\\x001 \\' '`",
      options: ["non-printable", { exact: true }],
      errors: [
        raw`Unexpected non-printable characters \0, \0, escaped printable characters \x20, \x41, \xC4, escape formatting`,
      ],
    },
    {
      code: raw`"\x00\x001\001"`,
      output: raw`"\0\x001\x01"`,
      options: ["non-printable", { exact: true }],
      errors: ["Unexpected escape formatting"],
    },

    {
      code: "`\n\\n\\x20\\x41 + B · Å_\\xc4 - \\uFfFe / \x00\x001 \\' '`",
      output: "`\n\\n A + B \\xb7 \\xc5_\\xc4 - \\ufffe / \\0\\x001 \\' '`",
      options: ["non-ascii", { exact: true }],
      errors: [
        raw`Unexpected non-ASCII characters \xb7, \xc5, \0, \0, escaped ASCII characters \x20, \x41, escape formatting`,
      ],
    },
    {
      code: raw`"\u000a"`,
      output: raw`"\n"`,
      options: ["non-ascii", { exact: true }],
      errors: ["Unexpected escape formatting"],
    },

    // It would be nicer to reformat these, to remove unnecessary escaped.
    // See issue #21.
    {
      code: "`\\x24{`",
      output: "`\\${`",
      options: ["non-ascii", { exact: true }],
      errors: ["Unexpected escape formatting"],
    },
    {
      code: "`$\\x7b`",
      output: "`$\\{`",
      options: ["non-ascii", { exact: true }],
      errors: ["Unexpected escape formatting"],
    },
    {
      code: "`\\x24\\x7b`",
      output: "`\\$\\{`",
      options: ["non-ascii", { exact: true }],
      errors: ["Unexpected escape formatting"],
    },
    {
      code: raw`"\x27\x22\x60\x24\x7b"`,
      output: raw`"\'\"\`\$\{"`,
      options: ["non-ascii", { exact: true }],
      errors: ["Unexpected escape formatting"],
    },

    {
      code: raw`"\x5c\x0a\x0d\x0b\x09\x08\x0c"`,
      output: raw`"\\\n\r\v\t\b\f"`,
      options: ["non-ascii", { exact: true }],
      errors: ["Unexpected escape formatting"],
    },

    // Surrogate pairs.
    {
      code: raw`"\ud800\udc00 \udbff\udfff \udfff\udbff"`,
      output: raw`"\u{10000} \u{10ffff} \udfff\udbff"`,
      options: ["non-ascii", { exact: true }],
      errors: ["Unexpected escape formatting"],
    },
    {
      code: raw`"\ud800\udc00 \udbff\udfff \udfff\udbff"`,
      output: '"\u{10000} \u{10ffff} \\udfff\\udbff"',
      options: ["non-printable", { exact: true }],
      errors: [
        raw`Unexpected escaped printable characters \ud800\udc00, \udbff\udfff`,
      ],
    },
    {
      code: "'\ud800\udc00 \udbff\udfff \udfff\udbff'",
      output: raw`'\u{10000} \u{10ffff} \udfff\udbff'`,
      options: ["non-ascii", { exact: true }],
      errors: [
        raw`Unexpected non-ASCII characters \u{10000}, \u{10ffff}, \udfff, \udbff`,
      ],
    },
    {
      code: "'\ud800\udc00 \\udbff\\udfff \udfff\udbff'",
      output: "'\u{10000} \u{10ffff} \\udfff\\udbff'",
      options: ["non-printable", { exact: true }],
      errors: [
        raw`Unexpected non-printable characters \udfff, \udbff, escaped printable character \udbff\udfff`,
      ],
    },
    {
      code: "'\\ud800\\udc00 \udbff\udfff \udfff\udbff'",
      output: "'\\ud800\\udc00 \u{10ffff} \\udfff\\udbff'",
      options: ["non-printable"],
      errors: [raw`Unexpected non-printable characters \udfff, \udbff`],
    },
  ].map((testCase) => {
    // Check that all strings evaluate to the same value.
    /* eslint-disable no-unused-vars */
    const tag = (arr, ...interpolated) => arr.join() + interpolated.join();
    const ä = "äää";
    /* eslint-enable no-unused-vars */

    const input = eval(testCase.code);
    const output = eval(testCase.output);

    if (input !== output) {
      console.error(
        "Test case input and output don't represent the same string",
        testCase
      );
      process.exit(1);
    }

    return testCase;
  }),
});
