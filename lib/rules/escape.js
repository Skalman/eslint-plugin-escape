const raw = String.raw;

const settings = {
  // Allowed: Carriage return, newline, tab, space, printable ASCII.
  // An unescaped carriage return in a JS file is interpreted as a
  // newline, but include it here anyway for completeness.
  "non-ascii": {
    name: "ASCII",
    reInvalid: /[^\r\n\t\x20-\x7e]/u,
  },

  "non-printable": {
    name: "printable",
    reInvalid: new RegExp(
      "[" +
        [
          raw`\0-\x08`,
          raw`\x0b\x0c`,
          raw`\x0e-\x1f`,
          raw`\x7f`,
          raw`\x85`,
          raw`\xa0`,
          raw`\xad`,
          raw`\u1680`,
          raw`\u2000-\u200b`,
          raw`\u200e`,
          raw`\u200f`,
          raw`\u2028\u2029`,
          raw`\u202f`,
          raw`\u205f`,
          raw`\u2060`,
          raw`\u3000`,
          raw`\ue000-\uf8ff`, // Private Use Area
          raw`\ud800-\udfff`, // Lone surrogates
          raw`\u{f0000}-\u{ffffd}`, // Supplementary Private Use Area-A
          raw`\u{100000}-\u{10fffd}`, // Supplementary Private Use Area-B
        ].join("") +
        "]",
      "u"
    ),
  },
};

module.exports = {
  meta: {
    type: "layout",

    docs: {
      description:
        "require non-printable or non-ASCII characters to be escaped in strings",
      category: "Formatting",
      recommended: true,
    },
    fixable: "code",
    schema: [
      {
        enum: ["non-printable", "non-ascii"],
      },
      {
        type: "object",
        properties: {
          exact: {
            type: "boolean",
          },
        },
        additionalProperties: false,
      },
    ],
  },

  supportsAutofix: true,

  create: (context) => {
    const { reInvalid, name } = settings[context.options[0] || "non-printable"];
    const reInvalidForReplace = new RegExp(reInvalid, "gu");
    const exact = context.options[1];
    const reEscaped = /\\(\\|x[\da-f]{2}|u[\da-f]{4}|u\{[\da-f]{1,6}\}|[0-3]?[0-7]{1,2})/gi;

    function handleEscapedChars(raw) {
      const hasEscapedChars = reEscaped.test(raw);

      if (!hasEscapedChars) return;

      const toUnescape = [];
      let fixedFormatting = false;

      const fixed = raw
        .replace(reEscaped, (match, escape, index) => {
          if (escape === "\\") {
            return match;
          }

          const codePoint =
            escape[0] === "u"
              ? parseInt(
                  escape[1] === "{" ? escape.slice(2, -1) : escape.slice(1),
                  16
                )
              : escape[0] === "x"
              ? parseInt(escape.slice(1), 16)
              : parseInt(escape, 8);

          const unescaped = String.fromCodePoint(codePoint);
          const replacement = createEscapeSequence(
            codePoint,
            raw,
            match,
            index
          );

          // If the char is valid, and it doesn't have a short escape code,
          // return the unescaped version.
          if (replacement.length !== 2 && !reInvalid.test(unescaped)) {
            toUnescape.push(match);
            return unescaped;
          } else {
            if (replacement !== match) {
              fixedFormatting = true;
            }
            return replacement;
          }
        })
        // Replace surrogate pairs with their combined escape sequence, or unescape them.
        .replace(
          /\\u(d[89ab][\da-f]{2})\\u(d[c-f][\da-f]{2})/g,
          (match, high, low) => {
            const unescaped = String.fromCharCode(
              parseInt(high, 16),
              parseInt(low, 16)
            );

            if (!reInvalid.test(unescaped)) {
              toUnescape.push(match);
              return unescaped;
            } else {
              const combinedHex = unescaped.codePointAt(0).toString(16);
              fixedFormatting = true;
              return `\\u{${combinedHex}}`;
            }
          }
        );

      if (!toUnescape.length && !fixedFormatting) {
        return;
      }

      return {
        fixed,
        invalidChars: toUnescape.length && toUnescape,
        format: fixedFormatting,
      };
    }

    function handleUnescapedChars(raw) {
      const isInvalid = reInvalid.test(raw);

      if (!isInvalid) return;

      const invalidChars = [];

      const fixed = raw.replace(reInvalidForReplace, (match, index) => {
        const replacement = createEscapeSequence(
          match.codePointAt(0),
          raw,
          match,
          index
        );

        invalidChars.push(replacement === "\\x00" ? "\\0" : replacement);
        return replacement;
      });

      return { invalidChars, fixed };
    }

    function process(node, raw, replacementRange) {
      const escaped = exact && handleEscapedChars(raw);
      if (escaped) {
        raw = escaped.fixed;
      }

      const unescaped = handleUnescapedChars(raw);
      if (unescaped) {
        raw = unescaped.fixed;
      }

      if (!escaped && !unescaped) {
        return;
      }

      const a = unescaped && unescaped.invalidChars;
      const b = escaped && escaped.invalidChars;
      const c = escaped && escaped.format;

      context.report({
        node,
        message:
          "Unexpected " +
          [
            a && `non-${name} character${s(a)} ${a.join(", ")}`,
            b && `escaped ${name} character${s(b)} ${b.join(", ")}`,
            c && "escape formatting",
          ]
            .filter((x) => x)
            .join(", "),
        fix(fixer) {
          return fixer.replaceTextRange(replacementRange, raw);
        },
      });
    }

    return {
      Literal(node) {
        if (typeof node.value === "string") {
          process(node, node.raw, node.range);
        }
      },

      TemplateElement(node) {
        // The node's range contains surrounding chars that the raw value
        // doesn't.
        const range = [
          // Start
          node.range[0] + 1,
          // End
          node.range[1] - (node.tail ? 1 : 2),
        ];

        process(node, node.value.raw, range);
      },
    };
  },
};

function s(array) {
  return array.length === 1 ? "" : "s";
}

function createEscapeSequence(codePoint, rawString, rawCodePoint, index) {
  if (codePoint <= 0xff) {
    if (codePoint === 0 && !/\d/.test(rawString[index + rawCodePoint.length])) {
      return "\\0";
    } else if (codePoint === 39) {
      return "\\'";
    } else if (codePoint === 34) {
      return '\\"';
    } else if (codePoint === 96) {
      return "\\`";
    } else if (codePoint === 36) {
      return "\\$";
    } else if (codePoint === 123) {
      return "\\{";
    } else if (codePoint === 92) {
      return "\\\\";
    } else if (codePoint === 10) {
      return "\\n";
    } else if (codePoint === 13) {
      return "\\r";
    } else if (codePoint === 11) {
      return "\\v";
    } else if (codePoint === 9) {
      return "\\t";
    } else if (codePoint === 8) {
      return "\\b";
    } else if (codePoint === 12) {
      return "\\f";
    } else {
      return "\\x" + codePoint.toString(16).padStart(2, "0");
    }
  } else if (codePoint <= 0xffff) {
    return "\\u" + codePoint.toString(16).padStart(4, "0");
  } else {
    return "\\u{" + codePoint.toString(16) + "}";
  }
}
