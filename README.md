# eslint-plugin-escape

Autofixable escaping for non-printable and non-ASCII characters in strings.

## Installation

You'll first need to install [ESLint](https://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-escape`:

```
$ npm i eslint-plugin-escape --save-dev
```

## Usage

Add `escape` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
  "plugins": ["escape"]
}
```

Then configure the rules you want to use under the rules section.

```json
{
  "rules": {
    "escape/escape": ["error", "non-printable"]
  }
}
```

## Options

This rule has two options, a string option and an object option.

String option:

- `"non-printable"` (default) requires non-printable characters to be escaped
- `"non-ascii"` requires non-ASCII characters to be escaped

Object option:

- `"exact": true` requires an exact formatting
  - use the shortest possible form (but never octal)
  - don't escape unnecessarily
  - use lower case hexadecimal escape codes

### non-printable

The `"non-printable"` option requires non-printable characters to be escaped inside of all strings. This is the default option.

Because they're non-printable, it's difficult to demonstrate the problem.

Instead, here are examples of **correct** code for this rule with the default `"non-printable"` option:

```js
/* eslint escape/escape: ["error", "non-printable"] */
var nullCharacter = "\0";
var distance = "100\xa0km";
```

### non-ascii

Examples of **incorrect** code for this rule with the `"non-ascii"` option:

```js
/* eslint escape/escape: ["error", "non-ascii"] */
var greeting = "¡Héłlõ, wöŕlð!";
```

Examples of **correct** code for this rule with the `"non-ascii"` option:

```js
/* eslint escape/escape: ["error", "non-ascii"] */
var greeting = "\xa1H\xe9\u0142l\xf5, w\xf6\u0155l\xf0!";
var distance = "100\xa0km";
```

### exact

Examples of **incorrect** code for this rule with the `"non-ascii", { "exact": true }` options:

```js
/* eslint escape/escape: ["error", "non-ascii", { "exact": true }] */
var space = "\x20";
var newline = "\u000a";
var replacement = "\uFFFD";
```

Examples of **correct** code for this rule with the `"non-ascii", { "exact": true }` options:

```js
/* eslint escape/escape: ["error", "non-ascii", { "exact": true }] */
var space = " ";
var newline = "\n";
var replacement = "\ufffd";
```

## License

Copyright Dan Wolff, [MPL 2.0](./LICENSE)
